<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Oyster\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name', null, array(
                    'label' => 'Your Name',
                    'required' => true
                )
            )

            ->add('surname', null, array(
                    'label' => 'Your Surname',
                    'required' => true
                )
            )

            ->add('status', 'checkbox', array(
                    'label' => 'Your Status',
                    'required' => false
                )
            )
            ->add('tag', 'entity', array(
                    'multiple' => true,
                    'expanded' => true,
                    'property' => 'tag',
                    'class' => 'Oyster\UserBundle\Entity\Tag',
                )
            );;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'oyster_user';
    }
}