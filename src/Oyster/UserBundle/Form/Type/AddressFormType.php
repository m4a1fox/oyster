<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Oyster\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AddressFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('country', null, array(
                    'label' => 'Country Name',
                )
            )

            ->add('city', null, array(
                    'label' => 'City Name',
                )
            )

            ->add('postCode', null, array(
                    'label' => 'Post Code',
                )
            )

            ->add('streetAddress', null, array(
                    'label' => 'Street Address',
                )
            );
    }

    public function getName()
    {
        return 'oyster_address';
    }
}