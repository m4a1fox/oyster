<?php

namespace Oyster\UserBundle\Controller;

/**
 * Forms. Start
 */
use Oyster\UserBundle\Entity\Pet;
use Oyster\UserBundle\Form\Type\PetFormType;
use Oyster\UserBundle\Form\Type\TagFormType;
use Oyster\UserBundle\Form\Type\UserFormType;
use Oyster\UserBundle\Form\Type\AddressFormType;
/**
 * Forms. End
 */
use Oyster\UserBundle\Entity\User as OysterUser;
use Oyster\UserBundle\Entity\Tag as OysterTag;
use Oyster\UserBundle\Entity\Address as OysterAddress;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class DefaultController extends Controller
{
    public function indexAction()
    {
        $getAllUsers = $this->container->get('oyster.user.get_user')->getAllUsers();

        return $this->render('OysterUserBundle:Default:index.html.twig', array('usersArray' => $getAllUsers));
    }

    public function editUserAction($id, Request $request)
    {
        if (!is_numeric($id)) {
            throw new NotFoundHttpException('Not found user');
        }

        $getUserData = $this->container->get('oyster.user.get_user')->getUserById($id);
        $form = $this->createForm(new UserFormType(), $getUserData);


        if ($request->getMethod() == 'POST') {
            $form->submit($request);
            if ($form->isValid()) {
                $this->container->get('oyster.user.get_user')->updateUser();

                return $this->redirect("/");
            } else {
                throw new NotFoundHttpException('Some Error.');
            }
        }

        return $this->render('OysterUserBundle:Default/edit:edit.html.twig',
            array(
                'form' => $form->createView(),
                'userId' => $id,
                'userData' => $getUserData
            )
        );
    }

    function addNewUserAction(Request $request)
    {
        $user = new OysterUser();
        $form = $this->createForm(new UserFormType(), $user);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);
            if ($form->isValid()) {
                $this->container->get('oyster.user.get_user')->addNewUser($user);

                return $this->redirect('/');
            }
        }

        return $this->render('OysterUserBundle:Default/add:add.html.twig', array('form' => $form->createView()));
    }


    function addNewTagAction(Request $request)
    {
        $tag = new OysterTag();
        $form = $this->createForm(new TagFormType(), $tag);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);
            if ($form->isValid()) {
                $this->container->get('oyster.user.get_tag')->addNewTag($tag);

                return $this->redirect('/');
            }
        }

        return $this->render('OysterUserBundle:Default/tag:add.html.twig', array('form' => $form->createView()));
    }

    function addNewUserAddressAction($userId, Request $request)
    {

        $address = new OysterAddress();
        $form = $this->createForm(new AddressFormType(), $address);
        $getUserData = $this->container->get('oyster.user.get_user')->getUserById($userId);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);
            if ($form->isValid()) {
                $this->container->get('oyster.user.add_address')->addNewAddress($address, $getUserData);

                return $this->redirect('/');
            }
        }

        return $this->render('OysterUserBundle:Default/address:add.html.twig', array('form' => $form->createView(), 'userId' => $userId));
    }

    function editUserAddressAction($addressId, Request $request)
    {
        if (!is_numeric($addressId)) {
            throw new NotFoundHttpException('Not found address');
        }

        $getAddressData = $this->container->get('oyster.user.add_address')->getAddressById($addressId);

        $form = $this->createForm(new AddressFormType(), $getAddressData);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);
            if ($form->isValid()) {
                $this->container->get('oyster.user.add_address')->updateAddress();

                return $this->redirect("/");
            } else {
                throw new NotFoundHttpException('Some Error.');
            }
        }

        return $this->render('OysterUserBundle:Default/address:edit.html.twig',
            array(
                'form' => $form->createView(),
                'addressId' => $addressId
            )
        );
    }
}
