<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 11/20/13 | 7:38 PM
 */


namespace Oyster\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    function getAllUsers()
    {
        return $this->findAll();
    }

    function getUserById($id) {
        return $this->findOneBy(array('id' => $id));
    }

    function updateUser()
    {
        $this->_em->flush();
    }

    function addNewUser($user){
        $this->_em->persist($user);
        $this->updateUser();
    }
}