<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 8/25/13 | 10:32 PM
 */


namespace Oyster\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;


class AddressRepository extends EntityRepository
{

    function addNewAddress($address, $user)
    {
        $address->setParentId($user);
        $this->_em->persist($address);
        $this->_em->flush();
    }

    function getAddressById($addressId)
    {
        return $this->findOneBy(array('id' => $addressId));
    }

    function updateAddress()
    {
        $this->_em->flush();
    }
}