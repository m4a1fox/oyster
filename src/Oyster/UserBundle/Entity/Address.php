<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 7/8/13 | 12:06 PM
 */


namespace Oyster\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="user_address")
 * @ORM\Entity(repositoryClass="Oyster\UserBundle\Entity\AddressRepository")
 */

class Address
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $country;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $streetAddress;


    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $postCode;

    /**
     * @ORM\ManyToOne(targetEntity="Oyster\UserBundle\Entity\User", inversedBy="userId")
     * @ORM\JoinColumn(name="parentId", referencedColumnName="id")
     */
    protected $parentId;


    /**
     * @ORM\Column(type="datetime", length=100, nullable=true)
     */
    protected $saveDate;

    /**
     * @ORM\Column(type="datetime", length=100, nullable=true)
     */
    protected $updateDate;

    /**
     * @ORM\PrePersist
     */
    public function preSave()
    {
        $this->setSaveDate(new \DateTime('now'));
        $this->setUpdateDate(new \DateTime('now'));
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->setUpdateDate(new \DateTime('now'));
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Address
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set streetAddress
     *
     * @param string $streetAddress
     * @return Address
     */
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    /**
     * Get streetAddress
     *
     * @return string 
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * Set postCode
     *
     * @param string $postCode
     * @return Address
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get postCode
     *
     * @return string 
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set parentId
     *
     * @param \Oyster\UserBundle\Entity\User $parentId
     * @return Address
     */
    public function setParentId(\Oyster\UserBundle\Entity\User $parentId = null)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return \Oyster\UserBundle\Entity\User
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set saveDate
     *
     * @param \DateTime $saveDate
     * @return Address
     */
    public function setSaveDate($saveDate)
    {
        $this->saveDate = $saveDate;

        return $this;
    }

    /**
     * Get saveDate
     *
     * @return \DateTime 
     */
    public function getSaveDate()
    {
        return $this->saveDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Address
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }
}
