<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 7/8/13 | 12:06 PM
 */


namespace Oyster\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="Oyster\UserBundle\Entity\TagRepository")
 */

class Tag
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $tag;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="tag")
     */
    private $userId;


    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userId = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return Tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Add userId
     *
     * @param \Oyster\UserBundle\Entity\User $userId
     * @return Tag
     */
    public function addUserId(\Oyster\UserBundle\Entity\User $userId)
    {
        $this->userId[] = $userId;

        return $this;
    }

    /**
     * Remove userId
     *
     * @param \Oyster\UserBundle\Entity\User $userId
     */
    public function removeUserId(\Oyster\UserBundle\Entity\User $userId)
    {
        $this->userId->removeElement($userId);
    }

    /**
     * Get userId
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
