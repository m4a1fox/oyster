<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 8/25/13 | 10:32 PM
 */


namespace Oyster\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;


class TagRepository extends EntityRepository
{

    function addNewTag($tag)
    {
        $this->_em->persist($tag);
        $this->_em->flush();
    }

    function getTagById($tagId)
    {
        return $this->findOneBy(array('id' => $tagId));
    }

    function updateTag()
    {
        $this->_em->flush();
    }
}