<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 11/20/13 | 7:38 PM
 */


namespace Oyster\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Oyster\UserBundle\Entity\UserRepository")
 */
class User {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $surname;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $status;

    /**
     * @ORM\OneToMany(targetEntity="Oyster\UserBundle\Entity\Address", mappedBy="parentId", cascade={"all"})
     */
    protected $userId;

    /**
     * @ORM\Column(type="datetime", length=100, nullable=true)
     */
    protected $saveDate;

    /**
     * @ORM\Column(type="datetime", length=100, nullable=true)
     */
    protected $updateDate;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="userId")
     * @ORM\JoinTable(name="user_tag")
     */
    private $tag;

    /**
     * @ORM\PrePersist
     */
    public function preSave()
    {
        $this->setSaveDate(new \DateTime('now'));
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->setUpdateDate(new \DateTime('now'));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set status
     *
     * @param \bool $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userId = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userId
     *
     * @param \Oyster\UserBundle\Entity\Address $userId
     * @return User
     */
    public function addUserId(\Oyster\UserBundle\Entity\Address $userId)
    {
        $this->userId[] = $userId;

        return $this;
    }

    /**
     * Remove userId
     *
     * @param \Oyster\UserBundle\Entity\Address $userId
     */
    public function removeUserId(\Oyster\UserBundle\Entity\Address $userId)
    {
        $this->userId->removeElement($userId);
    }

    /**
     * Get userId
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set saveDate
     *
     * @param \DateTime $saveDate
     * @return User
     */
    public function setSaveDate($saveDate)
    {
        $this->saveDate = $saveDate;

        return $this;
    }

    /**
     * Get saveDate
     *
     * @return \DateTime
     */
    public function getSaveDate()
    {
        return $this->saveDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return User
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Add tag
     *
     * @param \Oyster\UserBundle\Entity\Tag $tag
     * @return User
     */
    public function addTag(\Oyster\UserBundle\Entity\Tag $tag)
    {
        $this->tag[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \Oyster\UserBundle\Entity\Tag $tag
     */
    public function removeTag(\Oyster\UserBundle\Entity\Tag $tag)
    {
        $this->tag->removeElement($tag);
    }

    /**
     * Get tag
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTag()
    {
        return $this->tag;
    }
}
