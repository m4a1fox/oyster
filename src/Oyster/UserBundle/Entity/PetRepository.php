<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 8/25/13 | 10:32 PM
 */


namespace Oyster\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;


class PetRepository extends EntityRepository
{

    function addNewPet($pet, $user)
    {
        $pet->setParentId($user);
        $this->_em->persist($pet);
        $this->_em->flush();
    }

    function getPetById($petId)
    {
        return $this->findOneBy(array('id' => $petId));
    }

    function updatePet()
    {
        $this->_em->flush();
    }
}