<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 8/15/14 | 9:34 PM
 */


namespace Oyster\UserBundle\Service;

use Oyster\UserBundle\Entity\Pet;
use Symfony\Component\DependencyInjection\Container;

class OysterUserPet
{
    private $doctrine;
    private $container;
    private $petRepository;


    function __construct(Container $container)
    {
        $this->container = $container;
        $this->doctrine = $this->container->get('doctrine');
        $this->petRepository = $this->doctrine->getRepository('OysterUserBundle:Pet');
    }


    function addNewPet(Pet $pet, $userId)
    {
        $this->petRepository->addNewPet($pet, $userId);
    }

    function getPetById($petId)
    {
        return $this->petRepository->getPetById($petId);
    }

    function updatePet()
    {
        $this->petRepository->updatePet();
    }

} 