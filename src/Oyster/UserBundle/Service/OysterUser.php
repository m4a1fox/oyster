<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 7/9/13 | 6:19 PM
 */


namespace Oyster\UserBundle\Service;

use Symfony\Component\DependencyInjection\Container;
use Oyster\UserBundle\Entity\User;

class OysterUser {
    private $doctrine;
    private $container;
    private $userRepository;



    function __construct(Container $container){
        $this->container = $container;
        $this->doctrine = $this->container->get('doctrine');
        $this->userRepository = $this->doctrine->getRepository('OysterUserBundle:User');
    }

    public function getAllUsers() {
        return $this->userRepository->getAllUsers();
    }

    public function getUserById($id) {

        return $this->userRepository->getUserById($id);
    }

    function updateUser(){
        $this->userRepository->updateUser();
    }

    function addNewUser(User $user) {
        $this->userRepository->addNewUser($user);
    }
}