<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 7/9/13 | 6:19 PM
 */


namespace Oyster\UserBundle\Service;

use Oyster\UserBundle\Entity\Address;
use Symfony\Component\DependencyInjection\Container;

class OysterUserAddress {
    private $doctrine;
    private $container;
    private $addressRepository;



    function __construct(Container $container){
        $this->container = $container;
        $this->doctrine = $this->container->get('doctrine');
        $this->addressRepository = $this->doctrine->getRepository('OysterUserBundle:Address');
    }


    function addNewAddress(Address $address, $userId) {
        $this->addressRepository->addNewAddress($address, $userId);
    }

    function getAddressById($addressId) {
        return $this->addressRepository->getAddressById($addressId);
    }

    function updateAddress(){
        $this->addressRepository->updateAddress();
    }

}