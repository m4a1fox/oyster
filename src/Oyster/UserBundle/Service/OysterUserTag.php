<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 8/15/14 | 9:34 PM
 */


namespace Oyster\UserBundle\Service;

use Oyster\UserBundle\Entity\Tag;
use Symfony\Component\DependencyInjection\Container;

class OysterUserTag
{
    private $doctrine;
    private $container;
    private $tagRepository;


    function __construct(Container $container)
    {
        $this->container = $container;
        $this->doctrine = $this->container->get('doctrine');
        $this->tagRepository = $this->doctrine->getRepository('OysterUserBundle:Tag');
    }


    function addNewTag(Tag $tag)
    {
        $this->tagRepository->addNewTag($tag);
    }

    function getTagById($tagId)
    {
        return $this->tagRepository->getTagById($tagId);
    }

    function updateTag()
    {
        $this->tagRepository->updateTag();
    }

} 