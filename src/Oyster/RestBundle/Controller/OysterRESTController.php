<?php
/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 * @copyright maxim 8/13/14 | 10:13 PM
 */


namespace Oyster\RestBundle\Controller;

use Oyster\UserBundle\Entity\Pet;
use Oyster\UserBundle\Entity\Tag;
use Oyster\UserBundle\Form\Type\PetFormType;
use Oyster\UserBundle\Form\Type\TagFormType;
use Oyster\UserBundle\Form\Type\UserFormType;
use Oyster\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class OysterRESTController extends Controller
{

    /**
     * Get all users and their addresses from system.
     * @ApiDoc(
     *  resource=true,
     *  description="This will return the json or xml data of users and users addresses.",
     *  requirements={
     *      {"name"="_format", "requirement"="false", "dataType"="json|xml", "description"="Use header (Accept: application/{_format}) to get the data in correct format"},
     *      {"name"="_format", "requirement"="false", "dataType"="json|xml", "description"="Use header (Accept: application/{_format}) to get the data in correct format"}
     * }
     * )
     *
     * @return array
     * @View()
     * @Route("customers")
     */
    public function getUsersAction()
    {
        $users = $this->container->get('oyster.user.get_user')->getAllUsers();

        return array('users' => $users);
    }

    /**
     * Get data of specific user, by sent id parameter in link.
     * @ApiDoc(
     *  resource=true,
     *  description="This will return the data of specific user by id.",
     *  requirements={
     *      {"name"="_format", "requirement"="false", "dataType"="json|xml", "description"="Use header (Accept: application/{_format}) to get the data in correct format"},
     *      {"name"="id", "requirement"="true", "dataType"="integer", "description"="Set the user id."}
     * }
     * )
     *
     * @param User $user
     * @return array
     * @View()
     * @ParamConverter("user", class="OysterUserBundle:User")
     * @Route("customers/{id}")
     */
    public function getUserAction(User $user)
    {
        return array('user' => $user);
    }

    /**
     * Create a new user.
     * @ApiDoc(
     *  resource=true,
     *  description="This will create the new user.",
     *  input="Oyster\UserBundle\Form\Type\UserFormType",
     * statusCodes={
     *         200="Returned when successful",
     *         404={
     *           "Returned when the user is not found.",
     *           "Returned when validation fail."
     *         },
     *     },
     *  requirements={
     *      {"name"="_format", "requirement"="false", "dataType"="json|xml", "description"="Use header (Accept: application/{_format}) to get the data in correct format"}
     * }
     * )
     * @param Request $request
     * @return array
     * @View()
     * @Post("customers/create")
     */
    public function createUserAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new UserFormType(), $user);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);
            if ($form->isValid()) {
                $this->container->get('oyster.user.get_user')->addNewUser($user);

                return array('code' => 200);
            }
        }

        return array('code' => 404);
    }

    /**
     * Update data of specific user, by sent id parameter in post parameters. Note: if you need set status to 0, don't set the status value at all
     * @ApiDoc(
     *  resource=true,
     *  description="This will update the data of specific user by id.",
     *  statusCodes={
     *         200="Returned when successful",
     *         404={
     *           "Returned when the user is not found.",
     *           "Returned when validation fail."
     *         },
     *         400="The request cannot be fulfilled due to bad syntax."
     *     },
     *  requirements={
     *      {"name"="_format", "requirement"="false", "dataType"="json|xml", "description"="Use header (Accept: application/{_format}) to get the data in correct format"}
     *  },
     * parameters={
     *      {"name"="oyster_user[name]", "dataType"="string", "required"=true, "description"="New user name"},
     *      {"name"="oyster_user[surname]", "dataType"="string", "required"=true, "description"="New user surname"},
     *      {"name"="oyster_user[status]", "dataType"="boolean", "required"=false, "description"="Set value (1), if need turn on user, if you need turn off user, not send this at all."},
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="Set user id which you need to update."},
     *      {"name"="oyster_user[tag][]", "dataType"="string|integer", "required"=true, "description"="Add single tag to relation between user and tag table (table name: user_tag). Note: to add each tag separate, use (oyster_user[tag][]) every time for each line of POST query."},
     *  }
     * )
     *
     * @param Request $request
     * @return array
     * @View()
     * @Post("customers/update")
     */
    public function updateUserAction(Request $request)
    {

        $id = $request->get('id');
        $getUserData = $this->container->get('oyster.user.get_user')->getUserById($id);
        if (is_object($getUserData)) {
            $form = $this->createForm(new UserFormType(), $getUserData);

            if ($request->getMethod() == 'POST') {
                $form->submit($request);
                if ($form->isValid()) {
                    $this->container->get('oyster.user.get_user')->updateUser();

                    return array('code' => 200);
                }

                return array('code' => $form->getErrors());
            }
        } else {
            return array('status' => 404);
        }


    }

    /**
     * Delete data of specific user, by sent id parameter in post parameters.
     * Also will delete the data in user address table.
     * @ApiDoc(
     *  resource=true,
     *  description="This will delete the data of specific user by id.",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the user is not found.",
     *         400="The request cannot be fulfilled due to bad syntax."
     *     },
     *  requirements={
     *      {"name"="_format", "requirement"="false", "dataType"="json|xml", "description"="Use header (Accept: application/{_format}) to get the data in correct format"}
     *  },
     * parameters={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="Set user id which you need to update."},
     *  }
     * )
     *
     * @param Request $request
     * @return array
     * @View()
     * @Post("customers/delete")
     */
    public function deleteUserAction(Request $request)
    {

        $id = $request->get('id');
        if (is_int((int)$id)) {
            $getUserData = $this->container->get('oyster.user.get_user')->getUserById($id);

            if ($request->getMethod() == 'POST') {
                if (is_object($getUserData)) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($getUserData);
                    $em->flush();

                    return array('code' => 200);
                }

                return array('code' => 404);
            }
        }

        return array('code' => 400);
    }

    /**
     * Add additional oneToMany relation to User Entity. Added a home pet.
     * @ApiDoc(
     *  resource=true,
     *  description="This will add pet of user.",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the user is not found.",
     *         400="The request cannot be fulfilled due to bad syntax."
     *     },
     *  requirements={
     *      {"name"="_format", "requirement"="false", "dataType"="json|xml", "description"="Use header (Accept: application/{_format}) to get the data in correct format"}
     *  },
     * parameters={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="Set user id which you need to add a home pet."},
     *      {"name"="oyster_pet[name]", "dataType"="string", "required"=true, "description"="New pet name"},
     *      {"name"="oyster_user[type]", "dataType"="string", "required"=true, "description"="What is the home peg. Is it dog, or fish, or cat"}
     *  }
     * )
     *
     * @param Request $request
     * @return array
     * @View()
     * @Post("customers/pet/add")
     */
    public function addPedToUserAction(Request $request)
    {
        $id = $request->get('id');

        if (is_int((int)$id)) {

            $getUserData = $this->container->get('oyster.user.get_user')->getUserById($id);
            if (is_object($getUserData)) {
                $pet = new Pet();
                $form = $this->createForm(new PetFormType(), $pet);

                if ($request->getMethod() == 'POST') {
                    $form->submit($request);
                    if ($form->isValid()) {
                        $this->container->get('oyster.user.pet_user')->addNewPet($pet, $getUserData);

                        return array('code' => 200);
                    }
                }
            }

            return array('code' => 404);
        }

        return array('code' => 400);
    }

    /**
     * Add additional ManyToMany relation to User Entity. Added a tag to user.
     * @ApiDoc(
     *  resource=true,
     *  description="This will add tag to user.",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the user is not found.",
     *         400="The request cannot be fulfilled due to bad syntax."
     *     },
     *  requirements={
     *      {"name"="_format", "requirement"="false", "dataType"="json|xml", "description"="Use header (Accept: application/{_format}) to get the data in correct format"}
     *  },
     * parameters={
     *      {"name"="oyster_tag[tag]", "dataType"="string|integer", "required"=true, "description"="Set tag into tag table."},
     *  }
     * )
     *
     * @param Request $request
     * @return array
     * @View()
     * @Post("customers/tag/add")
     */
    function addNewTagAction(Request $request)
    {
        $tag = new Tag();
        $form = $this->createForm(new TagFormType(), $tag);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);
            if ($form->isValid()) {
                $this->container->get('oyster.user.get_tag')->addNewTag($tag);

                return array('code' => 200);
            }
        }

        return array('code' => 404);
    }
} 